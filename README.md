# PureCloud License Monitoring Tool (AWS version)

AWS Lambda Function that monitor (on request) license satus per org
It downloads all users Ids with their presence statuses in one array. In second array it gets userIds with assigned licenseType.\
It's up to the front end page to merge and analyze those data.

## Body for request
Use ```POST``` request with body:
```
{
    "clientId": "a3f41e28-da17...",
    "clientSecret": "SJ3oDumdn...",
    "env": "mypurecloud.ie",
    "token": "",
    "mergeOutput": false
}
```
where:

* clientId / clientSecret - OAuth settings for your PureCloud org
* env  - environment of your PureCloud org
* token - this is empty for the 1st query, save it from response and fill during next queries against the same organization.
* mergeOutput  - instead of 2 separate arrays, response is merged into 1 combined array

## Example outputs:
Below exmaple responses for 2 possible queries:

```mergeOutput: false```
```
{
    "listOfUsers": [
        {
            "id": "c6d5e6b1-486e-40f5-85a6-",
            "status": "Offline"
        },
[...]
  "listOfLicenses": [
        {
            "id": "c6d5e6b1-486e-40f5-85a6-",
            "licenses": [
                "engage3"
            ],
            "selfUri": "/api/v2/license/users/c6d5e6b1-486e-40f5-85a6-"
        },
]
    "token": "Zqt_7MR8jI-k6dd39-U...."
}
```

```mergeOutput: true```

```
{
    "entities": [
        {
            "id": "c6d5e6b1-486e-40f5-85a6-b48f4209f6c8",
            "status": "Offline",
            "licenses": [
                "engage3"
            ]
        },
        {
            "id": "8424a47c-96d1-4317-aefc-687f0c6c33c0",
            "status": "Offline",
            "licenses": [
                "engage3"
            ]
        },
        ]
    "token": "Zqt_7MR8jI-k6dd39-U...."
}
```

## Misc
* In case of wrong / expired token provided in POST query, tool will verify it (subject to change) and relogin with provided clientId / clientSecret.\
* No 429 handling implemented.



# Further developer settings

## Local Installation & Testing 
Use SAM CLI For Mac / Windows

* Install docker first.
* $ npm install --global aws-sam-local

## Test
To test application locally, use:

$ sam local invoke "test" -e event.json

### Test via sh command
$ sh test.sh