//TODO: return error in case of login failure

// Load the SDK for JavaScript
var AWS = require('aws-sdk');


// Set the region 
AWS.config.update({
    region: 'eu-west-1'
});


let platformClient = require('purecloud-platform-client-v2'); // PureCloud API 
let defer = require('promise-defer');
let client = platformClient.ApiClient.instance;


var _MERGE_OUTPUT;


var listOfUsers = [];   // list of Users retreived from PureCloud (raw)
var listOfLicenses = [];// list of User licenses from PureCloud (raw)

exports.handler = function (event, context, callback) {
    console.log('main process started');

    // make sure arrays are empty
    listOfUsers = [];   // list of Users retreived from PureCloud (raw)
    listOfLicenses = [];// list of User licenses from PureCloud (raw)

    if (!event.clientId || !event.clientSecret || !event.env) {
        let errMessage = 'missing one or more required input parameters clientId/clientSecret/env'
        console.error(errMessage);
        callback(new Error(errMessage));
        return
    }

    console.log(`clientId: ${event.clientId.substring(0, 15)}[...]`);
    console.log(`clientSecret: ${event.clientSecret.substring(0, 15)}[...]`);
    console.log(`set current env to ${event.env}`);
    console.log(`_MERGE_OUTPUT ${event.mergeOutput}`);

    // combine listOfUsers with listofLicenses arrays 
    if (event.mergeOutput) {
        _MERGE_OUTPUT = true;
    } else {
        _MERGE_OUTPUT = false;
    }

    client.setEnvironment(event.env);


    getToken(event.clientId, event.clientSecret, event.token)
        .then(function (resp) {

            console.log(`current token ${resp.token.substring(0, 15)}[...]`);


            // getUsers
            getUsers(listOfUsers, 100, 1).then(function (listOfUsers) {
                console.log(`all users retreived (${listOfUsers.length})`);
                // getLicenses for all users
                getLicenses(listOfLicenses, 100, 1).then(function (listOfLicenses) {
                    console.log(`all licenses retreived (${listOfLicenses.length})`);

                    console.log('process finished successfully');

                    if (_MERGE_OUTPUT) {
                        let result = equijoin(listOfUsers, listOfLicenses, "id", "id",
                            ({ status }, { id, licenses }) => ({ id, status, licenses }));


                        console.log(`merged array size (${result.length})`);

                        context.succeed({
                            'orgName': resp.orgName,
                            'entities': result,
                            'token': resp.token
                        });

                    } else {
                        context.succeed({
                            'orgName': resp.orgName,
                            'listOfUsers': listOfUsers,
                            'listOfLicenses': listOfLicenses,
                            'token': resp.token
                        });
                    };

                }).catch(function (err) {
                    console.log(err);
                    context.succeed({ 'error': err });
                    //callback(new Error(err));
                })

            }).catch(function (err) {
                context.succeed({ 'error': err });
            })

        })
        .catch(function (err) {
            context.succeed({ 'error': err });
        })


};

function getToken(clientId, clientSecret, token) {
    console.log('getToken');
    let apiInstance = new platformClient.TokensApi();
    return new Promise(function (resolve, reject) {
        if (token) {
            // verify token first
            console.log('verify token from cache...');
            client.setAccessToken(token);


            apiInstance.getTokensMe()
                .then((data) => {
                    console.log(`token is still valid`);

                    resolve({
                        'orgName': data.organization.name,
                        'token': token
                    });
                })
                .catch((err) => {
                    console.log('most probably token expired - relogin');
                    // Log-in to PureCloud
                    login(clientId, clientSecret).then(function (aToken) {
                        // get orgName
                        console.log('try to get orgName');
                        apiInstance.getTokensMe()
                            .then((data) => {
                                console.log(`orgName ${data.organization.name}`);
                                resolve({
                                    'orgName': data.organization.name,
                                    'token': aToken
                                });
                            })
                            .catch((err) => {
                                console.log('cannot read organizationName');
                                reject(err)
                            });
                    }).catch(function (err) {
                        reject(err);
                    })

                });

        } else {
            // Log-in to PureCloud
            console.log('token not received in request...');
            login(clientId, clientSecret).then(function (aToken) {
                apiInstance.getTokensMe()
                    .then((data) => {
                        console.log(`orgName ${data.organization.name}`);
                        resolve({
                            'orgName': data.organization.name,
                            'token': aToken
                        });
                    })
                    .catch((err) => {
                        console.error('cannot read organizationName');
                        reject(err)
                    });
            }).catch(function (err) {
                reject(err);
            })
        }
    })
}

function login(clientId, clientSecret) {
    console.log('login to PureCloud attempt...');
    return new Promise(function (resolve, reject) {
        client.loginClientCredentialsGrant(clientId, clientSecret)
            .then(function () {
                // set authenticated things
                console.log('logged-in!');
                // set token
                console.log('save token from response');
                let aToken = client.authentications["PureCloud OAuth"].accessToken;
                if (aToken) {
                    client.setAccessToken(aToken);
                    resolve(aToken);
                } else {
                    console.error('couldnt find token in PureCloud response');
                    reject();
                }

            })
            .catch(function (err) {
                console.log('error during loginClientCredentialsGrant:  ', err);
                reject(err);
            });
    })
}

// Get Users
function getUsers(_array, _pageSize, _pageNumber, _def) {
    console.log(`getUsers page:${_pageNumber}`)
    var deferred = _def || new defer();

    try {

        let apiInstance = new platformClient.UsersApi();
        var opts = {
            'pageSize': _pageSize,
            'pageNumber': _pageNumber,
            'expand': ["presence"]
        };

        apiInstance.getUsers(opts).then(function (resp) {
            resp.entities.forEach(function (item) {
                _array.push({
                    'id': item.id,
                    'status': item.presence.presenceDefinition.systemPresence
                });
            });

            if (_pageNumber >= resp.pageCount) {
                deferred.resolve(_array);
            } else {
                getUsers(_array, _pageSize, _pageNumber + 1, deferred);
            }

        }).catch(function (error) {
            console.log(error);
            deferred.reject(error);
        });
    } catch (error) {
        console.log(error);
        deferred.reject(error);
    }
    return deferred.promise;
};

// Get Licenses for Users
function getLicenses(_array, _pageSize, _pageNumber, _def) {
    console.log(`getLicenses page:${_pageNumber}`)
    var deferred = _def || new defer();

    try {

        let apiInstance = new platformClient.LicenseApi();
        let opts = {
            'pageSize': _pageSize, // Number | Page size
            'pageNumber': _pageNumber // Number | Page number
        };        
        apiInstance.getLicenseUsers(opts).then(function (resp) {          
            resp.entities.forEach(function (item) {
                _array.push(item);
            });

            if (_pageNumber >= resp.pageCount) {
                deferred.resolve(_array);
            } else {
                getLicenses(_array, _pageSize, _pageNumber + 1, deferred);
            }

        }).catch(function (error) {
            console.error('Failed to getLicenseUsers');
            console.log(error);
            deferred.reject(error);
        });
    } catch (error) {
        console.log(error);
        deferred.reject(error);
    }
    return deferred.promise;
};

// merge 2 arrays based on selected column
const equijoin = (xs, ys, primary, foreign, sel) => {
    const ix = xs.reduce((ix, row) => ix.set(row[primary], row), new Map);
    return ys.map(row => sel(ix.get(row[foreign]), row));
};
